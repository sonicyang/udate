#!/usr/bin/env python3
# ------------------------------------------------------------------------------
# Name:        udate.py
# Purpose:
# Author:      Sonic
#
# Created:     19/12/2016
# Version:     0.1 Convert local time to UTC or any as new_tzone,
#              default local time is UTC+8
#              0.2 Support NOW or NOW-3h50 command line argument
#              0.2.1 format correct
# ------------------------------------------------------------------------------
'''
doctest convert time to UTC
>>> dt, stop = to_dt('2016/12/20T20:14', 8)
>>> print(dt.astimezone(timezone.utc).strftime(''.join(_UDATE_OUTPUT[:stop])))
2016/12/20T12:14
>>> dt, stop = to_dt('2019-09-30T07:59:0', 9.5)
>>> print(dt.astimezone(timezone.utc).strftime(''.join(_UDATE_OUTPUT[:stop])))
2019/09/29T22:29
>>> dt, stop = to_dt('2017-11-23T19', 8)
>>> print(dt.astimezone(timezone.utc).strftime(''.join(_UDATE_OUTPUT[:stop])))
2017/11/23T11
'''

from datetime import datetime
from datetime import timezone
from datetime import timedelta
import argparse
import re

_UDATE_OUTPUT = ['%Y', '/%m', '/%d', 'T%H', ':%M']
_UDATE_LOCAL_TZ = 8


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('dt_str', type=str,
                        help='Input date and time as the formats '
                        'yyyy/mm/ddThh:mm:ss, yyyy-mm-dd:hh:mm:ss, '
                        'now or now-3h30')
    parser.add_argument('-z', '--tzone', type=int, default=_UDATE_LOCAL_TZ,
                        help='Support int type only,'
                             ' means that -9:30 should use -9.5 instead.'
                             ' The default setting is %d' % _UDATE_LOCAL_TZ)
    parser.add_argument('-o', '--out', type=str, default=_UDATE_OUTPUT,
                        nargs='+',
                        help='%s is the default setting. '
                             'The output format uses '
                             'Python strptime magic word.' %
                             ' '.join(_UDATE_OUTPUT).replace('%', '%%'))
    parser.add_argument('-n', '--new_tzone', type=int, default=0,
                        help='Change dt to the time zone as the argument.')
    parser.add_argument('-s', '--silk_mode', action="store_true",
                        help='Switch to SiLK time format. '
                             'Ignore the hour and/or '
                             'following output if no input')
    return parser.parse_args()


def to_dt(dt=None, tz=None):
    local_dt = None
    local_tz = timezone(timedelta(minutes=tz*60))
    date_names = [r'year', r'mon', r'day', r'hour', r'min', r'sec']
    now_names = [r'now', r'ns', r'nh', r'nm']
    stop = len(date_names)
    re_str = r'''(
                 (?P<%s>[nN][oO][wW]) #now
                     ((?P<%s>[+-])?(?P<%s>\d+) #now hour
                        ([hH](?P<%s>\d+))? #now min
                     )?
                 )|(
                 (?P<%s>\d{4})[-:/|]  #year
                 (?P<%s>1[0-2]|0?[1-9])[-:/|]  #mon
                 (?P<%s>3[01]|[0-2]?\d)  #day
                 ([T| :](?P<%s>\d+) #hour
                    ([-:|](?P<%s>\d+) #min
                        ([-:|](?P<%s>\d+))? #sec
                    )?
                 )?
                 )''' % tuple(now_names+date_names)
    re_obj = re.compile(re_str, re.X)
    re_ans = re_obj.search(dt)
    re_dict = re_ans.groupdict()
    if re_dict[now_names[0]]:
        t_list = [int(re_dict[n]) for n in now_names[2:]
                  if re_dict[n] is not None]
        if re_dict[now_names[1]] == '-':
            t_list = [i*-1 for i in t_list]
        _t = timedelta(**dict(zip(['hours', 'minutes'], t_list)))
        local_dt = datetime.now(tz=local_tz) + _t
    else:
        dt_list = [int(re_dict[dn])
                   for dn in date_names if re_dict[dn] is not None]
        stop = len(dt_list)
        local_dt = datetime(*dt_list, tzinfo=local_tz)
    # print(stop)
    # print(local_dt)
    return local_dt, stop


def main():
    _tz = timezone.utc
    args = get_args()
    if args.new_tzone != 0:
        _tz = timezone(timedelta(minutes=args.new_tzone*60))
    dt, stop = to_dt(args.dt_str, args.tzone)
    if args.silk_mode:
        args.out = args.out[:stop]
    print(dt.astimezone(_tz).strftime(''.join(args.out)))


if __name__ == '__main__':
    main()
